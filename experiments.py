#!/usr/bin/env python3

import os
import sys
import shutil
import subprocess
import glob
import time
import tempfile
from math import sqrt, log2
from multiprocessing import Pool

# Name of images to compress, one on each line. These images must do in IMAGES_DIR.
CHOSEN_IMGS  = '/home/sznurek/UICDDataSet/FILES'

# Source of images to compress. Results of compression will be available in subfolders.
IMAGES_DIR   = '/home/sznurek/UICDDataSet'

# Path to the compressor executable.
EXECUTABLE   = '/home/sznurek/Programming/Quantization/.cabal-sandbox/bin/Quantization'

# Where to put plot data and plots?
PLOTS_DIR    = '/home/sznurek/Programming/Quantization/Plots'

# Cached results file of image compression. See comment at the bottom, in main function.
RESULTS_FILE = '/home/sznurek/Programming/Quantization/results.txt'

# (d, levels)
EXPERIMENTS = [
    (1, 2),
    (1, 4),
    (1, 8),
    (1, 16),
    (1, 32),
    (1, 64),
    (1, 128),
    (1, 256),
    (2, 2),
    (2, 4),
    (2, 8),
    (2, 16),
    (2, 32),
    (2, 64),
    (2, 128),
    (2, 256),
    (2, 512),
    (2, 1024),
    (2, 2048),
    (2, 4096),
    (4, 2),
    (4, 4),
    (4, 8),
    (4, 16),
    (4, 32),
    (4, 64),
    (4, 128),
    (4, 256),
    (4, 512),
    (4, 1024),
    (4, 2048),
    (4, 4096),
    (4, 8192),
    (4, 2**14),
]

DS    = [1,2,4]
RANGE = [2**i for i in range(1,15)]

def experiment_dir(exp):
    d, level = exp
    path = os.path.join(IMAGES_DIR, "%d-%d" % (d, level))
    #os.system("mkdir %s" % path)
    return path

def run_experiment(exp, image, n):
    d, level = exp
    image_path = os.path.join(IMAGES_DIR, image)
    output_path = os.path.join(experiment_dir(exp), "%s.%d.png" % (image, n))
    os.system('rm %s' % output_path)
    print("Running exp %s, run %d on image %s" % (str(exp), n, image))
    cmd = '%s %s %s %d %d 2>/dev/null | tail -n 1' % (EXECUTABLE, image_path, output_path, level, d)
    start = time.perf_counter()
    result = subprocess.check_output(cmd, shell=True)
    end = time.perf_counter() - start

    data_output_path = os.path.join(experiment_dir(exp), "%s.%d.result" % (image, n))
    open(data_output_path, 'w').write("%f %f\n" % (float(result), end))

def run(k):
    e, image, i = k
    run_experiment(e, image, i)

def run_experiments(images):
    pool = Pool()

    for exp in EXPERIMENTS:
        os.system('mkdir %s' % experiment_dir(exp))

    for image in images:
        for i in range(3):

            pool.map(run, map(lambda e: (e, image, i), EXPERIMENTS))

def stdev(mean, xs):
    return sqrt( sum(map(lambda x: (x - mean) ** 2, xs)) / (len(xs)-1) )

def gather_data(images):
    results = {}
    for image in images:
        results[image] = {}
        for exp in EXPERIMENTS:
            errors, times = [], []
            for i in range(3):
                err, tim = open(os.path.join(experiment_dir(exp), "%s.%d.result" % (image, i))).read().split()
                errors.append(float(err))
                times.append(float(tim))

            mean_error = sum(errors) / 3
            mean_time  = sum(times) / 3
            stdev_error = stdev(mean_error, errors)
            stdev_time = stdev(mean_time, times)

            results[image][exp] = {'error': (mean_error, stdev_error), 'time': (mean_time, stdev_time)}

    return results

def average_data(results):
    average = {}

    for exps in results.values():
        for exp, res in exps.items():
            if exp not in average.keys():
                average[exp] = {'error': [], 'time': []}
            average[exp]['time'].append(res['time'][0])
            average[exp]['error'].append(res['error'][0])

    for exp, res in average.items():
        mean_time = sum(res['time']) / len(res['time'])
        mean_error = sum(res['error']) / len(res['error'])

        stdev_time = stdev(mean_time, res['time'])
        stdev_error = stdev(mean_error, res['error'])

        average[exp] = {'time': (mean_time, stdev_time), 'error': (mean_error, stdev_error)}

    return average

def plot_image_data(output_dir, image, image_results):
    results = {}
    for exp, res in image_results.items():
        d, levels = exp
        if d not in results.keys():
            results[d] = {'time': [], 'error': []}

        results[d]['time'].append((levels, res['time']))
        results[d]['error'].append((levels, res['error']))

    for d, vals in results.items():
        output_file_time = os.path.join(output_dir, '%s-%d-time.data' % (image, d))
        output_file_error = os.path.join(output_dir, '%s-%d-error.data' % (image, d))
        time_lines = ["%d %f %f" % (levels, m, s) for (levels, (m, s)) in sorted(vals['time'])]
        error_lines = ["%d %f %f" % (levels, m, s) for (levels, (m, s)) in sorted(vals['error'])]

        open(output_file_time, 'w').write("\n".join(time_lines))
        open(output_file_error, 'w').write("\n".join(error_lines))


def plot_data():
    results = eval(open(RESULTS_FILE).read())
    for image, res in results.items():
        plot_image_data(PLOTS_DIR, image, res)

    avg = average_data(results)
    plot_image_data(PLOTS_DIR, 'average', avg)

def run_gnuplot():
    files = glob.glob('%s/*.data' % PLOTS_DIR)

    for f in files:
        tmp = tempfile.NamedTemporaryFile()
        script = """
        set term postscript
        set output "%s"
        set logscale %s
        plot "%s" with yerrorbars
        """

        ls = "xy" if "error" in f else "x"
        script = script % ('%s.ps' % f, ls, f)

        tmp.write(script.encode())
        tmp.flush()
        print(script)
        print(tmp.name)
        os.system('gnuplot %s' % tmp.name)

def main():
    from pprint import pprint
    files = map(lambda s: s.strip(), open(CHOSEN_IMGS).readlines())
    if sys.argv[1] == 'run': # Run compression of images.
        run_experiments(files)
    elif sys.argv[1] == 'gather': # gather statistics to RESULTS_FILE
        data = gather_data(files)
        pprint(data)
        open(RESULTS_FILE, 'w').write(str(data))
    elif sys.argv[1] == 'plot_data': # Generate plot data in PLOTS_DIR
        plot_data()
    elif sys.argv[1] == 'gnuplot': # Generate plots in PLOTS_DIR
        run_gnuplot()

if __name__ == "__main__":
    main()
