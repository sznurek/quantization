{-# LANGUAGE BangPatterns, ExistentialQuantification #-}
module Data.QMonad where

import Control.Monad
import Control.Monad.Free
import Control.Monad.Free.Church
import qualified Data.Vector.Unboxed as VU
import System.Random.MWC

data QMonadF a = QLog !String !a
               | QRandom !(Int -> a)
               | QRandomVector !Int !Int !(VU.Vector Int -> a)
               | QTick !a
               | QGetTick !(Int -> a)
               | forall b. QIO !(IO b) (b -> a)

instance Functor QMonadF where
    fmap f (QLog s a)   = QLog s (f a)
    fmap f (QRandom r)  = QRandom (fmap f r)
    fmap f (QRandomVector a b r)  = QRandomVector a b (fmap f r)
    fmap f (QTick a)    = QTick (f a)
    fmap f (QGetTick t) = QGetTick (fmap f t)
    fmap f (QIO io g)   = QIO io (fmap f g)

type QM a = F QMonadF a

logQ :: String -> QM ()
logQ str = liftF (QLog str ())

randomInt :: QM Int
randomInt = liftF (QRandom id)

randomVector :: Int -> Int -> QM (VU.Vector Double)
randomVector dim maxi = liftF (QRandomVector dim maxi (VU.map fromIntegral))

tickQ :: QM ()
tickQ = liftF (QTick ())

getTickQ :: QM Int
getTickQ = liftF (QGetTick id)

logTickQ :: QM ()
logTickQ = do
    t <- getTickQ
    logQ ("Tick: " ++ show t)

ioQ :: IO a -> QM a
ioQ io = liftF (QIO io id)

randomSublist :: Int -> [a] -> QM ([a], [a])
randomSublist 0 xs = return (xs, [])
randomSublist n xs = do
    let l = length xs
    k' <- randomInt
    let k = k' `rem` l
    let (ys, r:zs) = splitAt k xs
    (rest, rest') <- randomSublist (n-1) (ys ++ zs)
    return (r:rest, rest')

interpretIO :: QM a -> IO a
interpretIO q = withSystemRandom (\g -> go g 0 (fromF q)) where
    go _ _ (Pure a) = return a
    go !g !t (Free (QLog str rest)) = putStrLn str >> go g t rest
    go !g !t (Free (QRandom f))     = do
        i <- uniform g
        go g t (f i)
    go !g !t (Free (QRandomVector dim maxi f)) = do
        vec <- uniformVector g dim :: IO (VU.Vector Int)
        go g t (f (VU.map (\i -> fromIntegral $ (abs i) `rem` (maxi + 1)) vec))
    go !g !t (Free (QTick rest)) = go g (t+1) rest
    go !g !t (Free (QGetTick f)) = go g t (f t)
    go !g !t (Free (QIO io f))   = io >>= \a -> go g t (f a)

