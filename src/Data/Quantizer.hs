{-# LANGUAGE BangPatterns, FlexibleInstances, DeriveGeneric, RankNTypes, ScopedTypeVariables #-}

module Data.Quantizer where

import GHC.Generics

import           Control.Monad (join, replicateM, (>=>), (<=<))
import           Control.Monad.ST
import           Data.List
import           Data.Maybe
import           Data.Foldable (toList)
import qualified Data.Set as S
import           Data.Hashable
import qualified Data.HashMap.Strict as HM
import qualified Data.HashSet as HS
import qualified Data.KdTree.Static as K
import qualified Data.KdTree.Dynamic as KD
import qualified Data.KdMap.Static as KM
import           Data.QMonad
import qualified Data.Sequence as SQ
import qualified Data.Vector as V
import qualified Data.Vector.Mutable as VM
import qualified Data.Vector.Unboxed as VU
import           Debug.Trace

import System.Random.MWC
import System.Random.MWC.CondensedTable

newtype V = V { unV :: VU.Vector Double } deriving (Show,Eq,Ord,Generic)

instance Hashable (VU.Vector Double) where
    hashWithSalt salt v = if VU.null v then salt
                                       else hash (VU.head v) `hashWithSalt` hashWithSalt salt (VU.tail v)
instance Hashable V

liftV :: (VU.Vector Double -> VU.Vector Double) -> V -> V
liftV f (V vs) = V (f vs)
{-# INLINE liftV #-}

liftV2 :: (VU.Vector Double -> VU.Vector Double -> VU.Vector Double) -> V -> V -> V
liftV2 f (V v1) (V v2) = V (f v1 v2)
{-# INLINE liftV2 #-}

ifZero :: V -> a -> a -> a
ifZero (V vs) a b = if VU.all (==0) vs then a else b
{-# INLINE ifZero #-}

(.+), (.-) :: V -> V -> V
v1 .+ v2 = ifZero v1 v2 $ ifZero v2 v1 $ liftV2 (VU.zipWith (+)) v1 v2
v1 .- v2 = liftV (VU.map (0-)) v2 .+ v1
{-# INLINE (.+) #-}
{-# INLINE (.-) #-}

(.*) :: Double -> V -> V
c .* v = liftV (VU.map (c*)) v
{-# INLINE (.*) #-}

vzero :: V
vzero  = V (VU.fromList [0])
{-# INLINE vzero #-}

inner :: V -> V -> Double
inner v1 v2 = ifZero v1 0.0 $ ifZero v2 0.0 $ VU.sum . VU.imap (\i a -> a * (v2' VU.! i)) $ v1' where
  (V v1', V v2') = (v1, v2)
{-# INLINE inner #-}

dist2 :: V -> V -> Double
dist2 (V a) (V b) = VU.sum . VU.imap (\i a -> (a - (b VU.! i))^2) $ a
{-# INLINE dist2 #-}

convexcombine :: (Double, V) -> (Double, V) -> V
convexcombine (d1, V v1) (d2, V v2) = V $ ifZero (V v1) v2 $ ifZero (V v2) v1 $ VU.imap (\i _ -> (d1*(v1 VU.! i) + d2*(v2 VU.! i))/(d1+d2)) (VU.replicate (max (VU.length v1) (VU.length v2)) (0 :: Double))


maxMeansIterations = 16
epsilon = 0.01

data MassPoint = MP { position :: !V, mass :: !Double } deriving (Show, Eq, Ord, Generic)

instance Hashable MassPoint

instance Monoid MassPoint where
  mempty = MP vzero 0
  mappend (MP v1 m1) (MP v2 m2) = MP (convexcombine (m1, v1) (m2, v2)) (m1+m2)

type Region = (V, [MassPoint])

lbgRemass :: [Region] -> QM [Region]
lbgRemass rs = return . map remass $ rs where
    remass (v, [])  = (v, [])
    remass (_, mps) = (position (mconcat mps), mps)

splitCodepoint :: V -> [V]
splitCodepoint v = [(1 + epsilon) .* v,  (1 - epsilon) .* v]

splitCodepoints :: [V] -> [V]
splitCodepoints = concatMap splitCodepoint

lbgRegroup :: V.Vector MassPoint -> [V] -> QM [Region]
lbgRegroup mps vs = return $ runST $ do
    result <- VM.replicate n SQ.empty :: (forall s. ST s (VM.STVector s (SQ.Seq MassPoint)))
    mapM_ (extendVector result) [0..V.length mps - 1]
    mapM (makeRegion result) [0..n-1]
  where
    vvs = V.fromList vs
    n = V.length vvs
    tree = KM.buildWithDist (\(V vs) -> VU.toList vs) dist2 (zip vs [0..])

    extendVector :: VM.STVector s (SQ.Seq MassPoint)
                 -> Int
                 -> ST s ()
    extendVector result i = do
        let mp = (V.!) mps i
        let ix = snd (KM.nearest tree (position mp))
        sq <- VM.read result ix
        VM.write result ix ((SQ.<|) mp sq)

    makeRegion :: VM.STVector s (SQ.Seq MassPoint)
               -> Int
               -> ST s Region
    makeRegion result ix = do
        sq <- VM.read result ix
        return ((V.!) vvs ix, toList sq)

lbgStep :: V.Vector MassPoint -> [V] -> QM [Region]
lbgStep mps vs = lbgRegroup mps vs >>= lbgRemass

lbgWhile :: ([Region] -> [Region] -> Bool) -> V.Vector MassPoint -> [V] -> QM [Region]
lbgWhile p mps vs = go maxMeansIterations (lbgRegroup mps vs) (lbgStep mps vs) where
    go 0 _ mrs   = mrs
    go n mrs' mrs = do
        logQ ("Step " ++ show n)
        rs' <- mrs'
        rs  <- mrs
        --logQ ("New regions: " ++ show rs)
        if p rs' rs then return rs
                    else go (n-1) (return rs) (lbgStep mps (map fst rs))

lbgInitial :: V.Vector MassPoint -> QM [Region]
lbgInitial xs = return [(position (V.foldl' mappend mempty xs), V.toList xs)]

discreteLog :: Int -> Int
discreteLog n = go 0 1 where
    go !k !p = if p >= n then k else go (k+1) (2*p)

masspointize :: [V] -> V.Vector MassPoint
masspointize vs = V.fromList . map snd . HM.toList . HM.fromListWith mappend . map (\v -> (v, MP v 1)) $ vs

lbg epsilon m xs = fmap regionToCodebook . iteration (discreteLog m) . lbgInitial $ mps where
    mps = masspointize xs
    iteration 0 !rs = rs
    iteration n !rs = iteration (n-1) (rs >>= \rs -> lbgWhile p mps (splitCodepoints (map fst rs)))
    p rs rs' = let errNew   = regionsError rs'
                   errOld   = regionsError rs
                   relative = (errNew - errOld) / errOld
                   toShow   = "ERR NEW: " ++ show errNew ++ " ERR OLD: " ++ show errOld
                              ++ " RELATIVE: " ++ show relative
               in  toShow `trace` (errNew == 0 || abs relative < epsilon)
    regionsError = sum . map regionError
    regionError (v, mps) = sum . map (\(MP p m) -> m * dist2 v p) $ mps

data Codebook = Codebook
                { steps  :: !Int
                , midpoints  :: !(V.Vector V)
                , kdtree :: !(KM.KdMap Double V Int)
                } deriving Show

quantize :: Codebook -> V -> Int
quantize (Codebook _ _ t) x = snd (KM.nearest t x)

dequantize :: Codebook -> Int -> V
dequantize cb y = midpoints cb V.! y

regionToCodebook :: [Region] -> Codebook
regionToCodebook rs = Codebook (length rs) (V.fromList (map fst rs)) tree where
  tree = KM.buildWithDist (\(V v) -> VU.toList v) dist2 (zip (map fst rs) [0..])

showCodebook :: Codebook -> String
showCodebook (Codebook n mids _) = concatMap (\v -> showV v ++ "\n") . sort . map unV . V.toList $ mids where
    showV = concatMap (\d -> show d ++ " ") . VU.toList
