{-# LANGUAGE FlexibleContexts, RankNTypes #-}

import Data.Array.Repa hiding ((++), map)
import qualified Data.Array.Repa as R
import qualified Data.Array.Repa.Repr.Vector as RV
import Data.Array.Repa.Repr.ForeignPtr
import Data.Array.Repa.IO.DevIL
import qualified Data.Quantizer as Q
import qualified Data.Vector as V
import qualified Data.Vector.Unboxed as VU
import qualified Data.Map.Strict as M
import Debug.Trace
import Data.Word
import System.Random

import Data.QMonad

import System.Environment (getArgs)

type Img r = Array r DIM3 Word8
type ImgComponent r = Array r DIM2 Word8

toUnboxedArr :: (Monad m, Shape sh) => Array F sh Word8 -> m (Array U sh Word8)
toUnboxedArr arr = computeP (fromFunction (extent arr) (\ix -> index arr ix))

prepareImage :: Image -> IO (Img U)
prepareImage (RGB arr) = toUnboxedArr arr
prepareImage _         = error "unsupported image type"

getComponent :: Source r Word8 => Int -> Img r -> ImgComponent D
getComponent c arr = slice arr (Z :. All :. All :. c)

msqError arr1 arr2 = fmap (sqrt . normalize) $ (R.sumAllP (R.map ((^2) . (/256)) (farr1 -^ farr2))) where
    toDouble x = fromIntegral x :: Double
    (farr1, farr2) = (R.map toDouble arr1, R.map toDouble  arr2)
    Z :. w :. h :. c = extent arr1
    normalize x = x / fromIntegral (w*h*c)

cross :: [a] -> [a] -> [(a,a)]
cross xs ys = (,) <$> xs <*> ys

getSquare :: Source r Word8 => Int -> ImgComponent r -> (Int, Int) -> VU.Vector Word8
getSquare d arr (i,j) = square where
  Z :. w :. h = extent arr
  safeIndex (x, y) = if x >= w || y >= h then index arr (Z :. min (w-1) x :. min (h-1) y) else index arr (Z :. x :. y)
  square = VU.fromList . map safeIndex $ cross [i*d..(i+1)*d-1] [j*d..(j+1)*d-1]

allSquares :: Source r Word8 => Int -> ImgComponent r -> [VU.Vector Word8]
allSquares d arr = map (getSquare d arr) (cross [0..w'-1] [0..h'-1]) where
  Z :. w :. h = extent arr
  (w', h') = ((w-1) `div` d + 1, (h-1) `div` d + 1)

mapSquares :: Source r Word8
           => Int
           -> (VU.Vector Word8 -> VU.Vector Word8)
           -> ImgComponent r
           -> ImgComponent D
mapSquares d f arr = fromFunction (extent arr) get where
    Z :. w :. h = extent arr
    (w', h') = ((w-1) `div` d + 1, (h-1) `div` d + 1)
    cache = computeS $ fromFunction (Z :. w' :. h') (\(Z :. x :. y) ->
        f . getSquare d arr $ (x, y)) :: Array RV.V DIM2 (VU.Vector Word8)
    get (Z :. x :. y) = let (sx, sy) = (x `div` d, y `div` d)
                            (i, j)   = (x `rem` d, y `rem` d)
                      in (VU.!) (index cache $ Z :. sx :. sy) (d*i + j)

eachComponent :: (Source r Word8, Source r' Word8, Monad m)
              => (forall r''. Source r'' Word8 => ImgComponent r'' -> m (ImgComponent r'))
              -> Img r -> m (Img D)
eachComponent f img = do
    components <- sequence . map (\i -> f (getComponent i img)) $ [0..2]
    return $ fromFunction (extent img) (\(Z :. x :. y :. c) -> (components !! c) ! (Z :. x :. y))

quantizeComponent :: Int -> Int
                  -> (forall r. Source r Word8 => ImgComponent r -> QM (ImgComponent U))
quantizeComponent levels d arr = do
    let squares = allSquares d arr
    codebook <- Q.lbg 0.001 levels (map (Q.V . VU.map fromIntegral) $ squares)
    let f = VU.map round . Q.unV . Q.dequantize codebook . Q.quantize codebook . Q.V . VU.map fromIntegral
    computeP (codebook `seq` mapSquares d f arr)

quantizeImage :: Int -> Int -> String -> IO (Img U, Img D)
quantizeImage levels d filePath = do
  image <- (runIL (readImage filePath)) >>= prepareImage
  quantized <- interpretIO (eachComponent (quantizeComponent levels d) image)
  return (image, quantized)

main :: IO ()
main = do
  [filePath, writePath, levelsStr, dStr] <- getArgs
  let levels = read levelsStr :: Int
  let d = read dStr :: Int
  (image, quantizedD) <- quantizeImage levels d filePath
  putStrLn "REPA working..."
  quantizedF <- copyP quantizedD :: IO (Img F)
  let quantized = quantizedF
  err <- msqError image quantized
  putStrLn (show err)
  quantizedF <- copyP quantized
  runIL (writeImage writePath (RGB quantizedF))
